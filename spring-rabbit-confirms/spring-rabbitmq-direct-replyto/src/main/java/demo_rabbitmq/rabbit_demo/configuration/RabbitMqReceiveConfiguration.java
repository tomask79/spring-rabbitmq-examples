package demo_rabbitmq.rabbit_demo.configuration;

import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import demo_rabbitmq.rabbit_demo.receiver.RabbitMqReceiver;
import demo_rabbitmq.rabbit_demo.sender.RabbitMqSender;

@Configuration
public class RabbitMqReceiveConfiguration {
	
	{
		System.out.println("Creating receiving configuration.");
	}
	
	@Bean
	public ConnectionFactory connectionFactory() {
		CachingConnectionFactory connectionFactory = 
				new CachingConnectionFactory("localhost");
		connectionFactory.setUsername("guest");
		connectionFactory.setPassword("guest");
		connectionFactory.setPublisherConfirms(true);
		connectionFactory.setPublisherReturns(true);
		return connectionFactory;
	}

	@Bean
	public Queue greeting() {
		return new Queue("greeting");
	}
	
    MessageListener receiver() {
        return new MessageListenerAdapter(new RabbitMqReceiver(), "onMessage");
    }
    
    @Bean
	public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
		RabbitTemplate template = new RabbitTemplate(connectionFactory);
		template.setExchange("test_exchange");
		template.setRoutingKey("greeting2");
		template.setReplyTimeout(60000);
		template.setMandatory(true);
		return template;
	}
	
	@Bean
	public SimpleMessageListenerContainer serviceListenerContainer(
			ConnectionFactory connectionFactory) {
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
		container.setConnectionFactory(connectionFactory);
		container.setQueues(greeting());
		container.setMessageListener(receiver());
		container.setDefaultRequeueRejected(false);
		return container;
	}
	
	@Bean
	public RabbitMqSender sender() {
		final RabbitMqSender sender = new RabbitMqSender();
		return sender;
	}
}
