package demo_rabbitmq.rabbit_demo.sender;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Tomas Kloucek
 */
public class RabbitMqSender {

	@Autowired
	private RabbitTemplate template;
	
	public void send() {
		Message message = MessageBuilder.withBody("TomasKloucek".getBytes())
				.setContentType("text/plain")
				.build();

		// greeting
		template.setConfirmCallback(new RabbitTemplate.ConfirmCallback() {
			@Override
			public void confirm(CorrelationData correlationData, boolean ack, String cause) {
				System.out.println("Correlation data: "+correlationData.toString()+ " with status " +ack+ " cause: "+cause);
			}
		});

		template.setReturnCallback(new RabbitTemplate.ReturnCallback() {
			@Override
			public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
				System.out.println("Unrouted message: "+message.toString()+ " reply code: "+replyCode+" routingKey: "+routingKey);
			}
		});
		Message reply = this.template.sendAndReceive(message, new CorrelationData("abc"));
		System.out.println("Reply from server is: "+new String(reply.getBody()));
	}
}

