package demo_rabbitmq.rabbit_demo.configuration;

import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.AsyncRabbitTemplate;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import demo_rabbitmq.rabbit_demo.receiver.RabbitMqReceiver;
import demo_rabbitmq.rabbit_demo.sender.RabbitMqSender;

@Configuration
public class RabbitMqReceiveConfiguration {
	
	{
		System.out.println("Creating receiving configuration.");
	}
	
	@Bean
	public ConnectionFactory connectionFactory() {
		CachingConnectionFactory connectionFactory = 
				new CachingConnectionFactory("localhost");
		connectionFactory.setUsername("guest");
		connectionFactory.setPassword("guest");
		return connectionFactory;
	}
			
	@Bean
	public Queue greeting() {
		return new Queue("greeting");
	}

	@Bean
    public MessageListener receiver() {
        return new MessageListenerAdapter(new RabbitMqReceiver(), "onMessage");
    }

	@Bean
	public AsyncRabbitTemplate asyncRabbitTemplate(final ConnectionFactory connectionFactory) {
		return new AsyncRabbitTemplate(connectionFactory, "test_exchange","greeting",
				"replies", "reply_exchange/replies");
	}
	
	@Bean
	public SimpleMessageListenerContainer serviceListenerContainer(
			ConnectionFactory connectionFactory) {
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
		container.setConnectionFactory(connectionFactory);
		container.setQueues(greeting());
		container.setMessageListener(receiver());
		return container;
	}
	
	@Bean
	public RabbitMqSender sender() {
		return new RabbitMqSender();
	}
}
