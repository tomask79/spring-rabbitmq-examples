package demo_rabbitmq.rabbit_demo.sender;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.AsyncRabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.util.UUID;

/**
 * @author Tomas Kloucek
 */
public class RabbitMqSender {

	@Autowired
	private AsyncRabbitTemplate asyncRabbitTemplate;
	
	public void send() {
		Message requestMessage = MessageBuilder.withBody("Tomas Kloucek".getBytes())
				.setContentType("text/plain")
				.build();

		requestMessage.getMessageProperties().setMessageId(UUID.randomUUID().toString());
		System.out.println("I'm about to send message with ID: ["+requestMessage.getMessageProperties().getMessageId()+" ]");
		AsyncRabbitTemplate.RabbitMessageFuture reply = this.asyncRabbitTemplate.sendAndReceive(requestMessage);
		reply.addCallback(new ListenableFutureCallback<Message>() {
			@Override
			public void onFailure(Throwable throwable) {
				throwable.printStackTrace();
			}

			@Override
			public void onSuccess(Message message) {
				System.out.println("Server returned: "+new String(message.getBody()));
			}
		});
		System.out.println("Send method end.");
	}
}

