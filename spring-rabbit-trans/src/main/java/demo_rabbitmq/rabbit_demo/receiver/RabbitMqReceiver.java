package demo_rabbitmq.rabbit_demo.receiver;


import demo_rabbitmq.rabbit_demo.services.ItemsService;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Tomas Kloucek
 */
public class RabbitMqReceiver {

	@Autowired
	protected ItemsService itemsService;

	@Transactional
	public void receive(String item) throws Exception{
		System.out.println(" [x] Received '" + item + "'");
		throw new Exception("Error");
		//itemsService.processItem(item);
	}

}
