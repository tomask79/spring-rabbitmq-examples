package demo_rabbitmq.rabbit_demo.configuration;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import demo_rabbitmq.rabbit_demo.annotations.Sender;
import demo_rabbitmq.rabbit_demo.sender.RabbitMqSender;

@Configuration
@Sender
public class RabbitMqConfiguration {
	{
		System.out.println("Creating sender config...");
	}
	
	@Bean
	public ConnectionFactory connectionFactory() {
		CachingConnectionFactory connectionFactory = 
				new CachingConnectionFactory("localhost");
		connectionFactory.setUsername("guest");
		connectionFactory.setPassword("guest");
		return connectionFactory;
	}
	
	@Bean
	public AmqpAdmin amqpAdmin() {
		return new RabbitAdmin(connectionFactory());
	}

	@Bean
	public RabbitTemplate rabbitTemplate() {
		RabbitTemplate template = new RabbitTemplate(connectionFactory());
		template.setExchange("test_exchange");
		return template;
	}

	@Bean(initMethod="send")
	public RabbitMqSender sender() {
		final RabbitMqSender sender = new RabbitMqSender();
		return sender;
	}
}
