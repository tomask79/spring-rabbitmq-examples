package demo_rabbitmq.rabbit_demo.services;

import demo_rabbitmq.rabbit_demo.entity.Item;
import demo_rabbitmq.rabbit_demo.repository.ItemsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.ThreadLocalRandom;


/**
 * Created by tomas.kloucek on 27.1.2017.
 */
public class ItemsService {

    private final static Logger logger = LoggerFactory.getLogger(ItemsService.class);

    @Autowired
    public ItemsRepository repository;

    public void processItem(String itemName) {
        final Item item = new Item();
        item.setId(ThreadLocalRandom.current().nextInt());
        item.setItemName(itemName);
        repository.save(item);
    }
}