package demo_rabbitmq.rabbit_demo.repository;

import demo_rabbitmq.rabbit_demo.entity.Item;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by tomas.kloucek on 27.1.2017.
 */
public interface ItemsRepository extends JpaRepository<Item, Integer> {
}
