package demo_rabbitmq.rabbit_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Demo of rabbitmq
 */
@SpringBootApplication
@EnableTransactionManagement
public class App
{
    public static void main(String[] args) throws Exception {
        SpringApplication.run(App.class, args);
    }
}
