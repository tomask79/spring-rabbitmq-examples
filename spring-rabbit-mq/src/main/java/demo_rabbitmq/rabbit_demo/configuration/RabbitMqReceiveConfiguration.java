package demo_rabbitmq.rabbit_demo.configuration;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import demo_rabbitmq.rabbit_demo.annotations.Receiver;
import demo_rabbitmq.rabbit_demo.receiver.RabbitMqReceiver;

@Receiver
@Configuration
public class RabbitMqReceiveConfiguration {
	
	{
		System.out.println("Creating receiving configuration.");
	}
	
	@Bean
	public ConnectionFactory connectionFactory() {
		CachingConnectionFactory connectionFactory = 
				new CachingConnectionFactory("localhost");
		connectionFactory.setUsername("guest");
		connectionFactory.setPassword("guest");
		return connectionFactory;
	}
	
	@Bean
	public Queue fruits() {
		return new Queue("fruits");
	}
	
	@Bean
	public Queue vegetables() {
		return new Queue("vegetables");
	}
	
	@Bean
	public RabbitMqReceiver receiver() {
		return new RabbitMqReceiver();
	}
}
