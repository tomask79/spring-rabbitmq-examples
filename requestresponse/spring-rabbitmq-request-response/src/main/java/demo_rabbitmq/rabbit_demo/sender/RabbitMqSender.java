package demo_rabbitmq.rabbit_demo.sender;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;

/**
 * @author Tomas Kloucek
 */
public class RabbitMqSender {

	@Autowired
	private RabbitTemplate template;

	@Autowired
	private AmqpAdmin amqpAdmin;

	@Resource(name="replyContainer")
	SimpleMessageListenerContainer replyContainer;
	
	public void send() {
		Message message = MessageBuilder.withBody("TomasKloucek".getBytes())
				.setContentType("text/plain")
				.build();

		//Queue tempQueue = amqpAdmin.declareQueue();
		//template.setReplyAddress(tempQueue.getName());
		//replyContainer.setQueues(tempQueue);

		Message reply = this.template.sendAndReceive(message);
		System.out.println("Reply from server is: "+new String(reply.getBody()));
	}
}

