package demo_rabbitmq.rabbit_demo.receiver;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.ChannelAwareMessageListener;

public class RabbitMqReceiver implements ChannelAwareMessageListener{

	public String onMessage(String personName) {
		return "Hello "+personName;
	}

	@Override
	public void onMessage(Message message, Channel channel) throws Exception {
		channel.basicAck();
	}
}
